from pymongo import MongoClient

MONGO_URI = 'mongodb://localhost'

client = MongoClient(MONGO_URI)

bd = client['pifinancial-1']

collection_bots = bd['bots']



def get_bots():
    results = collection_bots.find()
    bots = []
    for r in results:
        bots.append(r)
    return bots 

def op_totales_bots():
    bots = get_bots()
    cant_op = 0
    for b in bots:
        cant_op += b['cant_op_realizadas']
    return cant_op

def existe_bot(name):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == name):
            return True
    return False

def get_ticker(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['ticker']
    return "Bot no encontrado"

def get_nombre_algoritmo(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['nombre_algoritmo']
    return "Bot no encontrado"

def get_nombre_moneda_secundaria(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['nombre_moneda_secundaria']
    return "Bot no encontrado"

def get_capital_inicial(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['capital_inicial']
    return "Bot no encontrado"

def get_capital_entrante(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['capital_entrante']
    return "Bot no encontrado"

def get_capital_disponible(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['capital_disponible']
    return "Bot no encontrado"

def get_capital_moneda_secundaria(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['capital_moneda_secundaria']
    return "Bot no encontrado"

def get_capital_total(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['capital_total']
    return "Bot no encontrado"

def get_capital_inicio_semana(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['capital_inicio_semana']
    return "Bot no encontrado"

def get_compra(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['compra']
    return "Bot no encontrado"

def get_telegram_api_key(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['telegram_api_key']
    return "Bot no encontrado"

def get_cant_op_realizadas(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['cant_op_realizadas']
    return "Bot no encontrado"

def get_obs_de_mercado(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['obs_de_mercado']
    return "Bot no encontrado"

def get_suscriptores_telegram(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['suscriptores_telegram']
    return "Bot no encontrado"

def update_suscriptores_telegram(nombre_bot, lista_suscriptores):
    try:
        collection_bots.update_one({"nombre_bot":nombre_bot} , {"$set": {"suscriptores_telegram":lista_suscriptores}})
    except:
        print("Error")

def update_cant_op_realizadas(nombre_bot, cant):
    try:
        collection_bots.update_one({"nombre_bot":nombre_bot} , {"$set": {"cant_op_realizadas":cant}})
    except:
        print("Error")

def update_capital_disponible(nombre_bot, valor):
    try:
        collection_bots.update_one({"nombre_bot":nombre_bot} , {"$set": {"capital_disponible":valor}})
    except:
        print("Error")

def update_capital_moneda_secundaria(nombre_bot, valor):
    try:
        collection_bots.update_one({"nombre_bot":nombre_bot} , {"$set": {"capital_moneda_secundaria":valor}})
    except:
        print("Error")

def update_capital_inicio_semana(nombre_bot, valor):
    try:
        collection_bots.update_one({"nombre_bot":nombre_bot} , {"$set": {"capital_inicio_semana":valor}})
    except:
        print("Error")

def update_capital_total(nombre_bot, valor):
    try:
        collection_bots.update_one({"nombre_bot":nombre_bot} , {"$set": {"capital_total":valor}})
    except:
        print("Error")

def update_capital_entrante(nombre_bot, valor):
    try:
        collection_bots.update_one({"nombre_bot":nombre_bot} , {"$set": {"capital_entrante":valor}})
    except:
        print("Error")

def update_capital_inicial(nombre_bot, valor):
    try:
        collection_bots.update_one({"nombre_bot":nombre_bot} , {"$set": {"capital_inicial":valor}})
    except:
        print("Error")

def update_compra(nombre_bot, compra):
    try:
        collection_bots.update_one({"nombre_bot":nombre_bot} , {"$set": {"compra":compra}})
    except:
        print("Error")

def update_obs_de_mercado(nombre_bot, datos):
    try:
        collection_bots.update_one({"nombre_bot":nombre_bot} , {"$set": {"obs_de_mercado":datos}})
    except:
        print("Error")

def get_ultimo_precio(nombre_bot):
    results = collection_bots.find()
    for r in results:
        if(r['nombre_bot'] == nombre_bot):
            return r['obs_de_mercado']['ultimo_precio']
    return "Bot no encontrado"




