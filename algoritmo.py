from mecanismos.mec_stop_loss import mec_stop_loss
import math
from indicadores.controlador_indicadores import Controlador_indicadores

class algoritmo:
    def __init__(self, nombre, obs_de_mercado):
        self.nombre = nombre
        self.obs_de_mercado = obs_de_mercado
        self.controlador_indicadores_15_min = Controlador_indicadores()
        self.controlador_indicadores_5_min = Controlador_indicadores()
        self.bot = ""

    def añadir_bot(self, bot):
        self.bot = bot
        self.mecanismo_stop_loss = mec_stop_loss(self.bot)
        

    def ejecutar_algoritmo_15_min(self):
        retorno = ['','']
        lista_precios = self.obs_de_mercado.get_lista_precios_15_min()
        self.controlador_indicadores_15_min.calcularIndicadores(lista_precios, lista_precios[-6:], self.obs_de_mercado.precios_15_min[-21:])
        
        ############### Copmpra  #################
        
        ############### Venta  ###################         
        # VENTA STOPLOSE ####
        if(self.bot.billetera.compra_abierta()):
            self.ganancia_operacion = self.bot.billetera.compra[-1].calcularPorcentajeDeGananciaDeOperacion(self.obs_de_mercado.get_ultimo_precio())
            retorno = self.mecanismo_stop_loss.ejecutar_mecanismo(self.ganancia_operacion)
            if (retorno != None and retorno[0] == 'venta'):
                return retorno
        

    
    def ejecutar_algoritmo_5_min(self):
        retorno = ['','']
        lista_precios = self.obs_de_mercado.get_lista_precios_5_min()
        self.controlador_indicadores_5_min.calcularIndicadores(lista_precios, lista_precios[-6:], self.obs_de_mercado.precios_5_min[-21:])

  

    

