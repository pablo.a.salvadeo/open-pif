# PIFinancial.com.ar Open

Este repositorio está pensado para posibilitar el desarrollo de un sistema de trading algorítmico. Se lo puede ver como la cáscara (envoltorio) y algunas herramientas necesarias para el desarrollo de los mecanismos que definiran el comportamiento de un bot de trading. 

<a href="https://www.repostatus.org/#active"><img src="https://www.repostatus.org/badges/latest/active.svg" alt="Project Status: Active – The project has reached a stable, usable state and is being actively developed." /></a> El proyecto ha alcanzado un estado estable y utilizable y se está desarrollando activamente.

Estamos contruyendo la documentación necesaria.

Nuestro sitio web también se encuentra en construcción.
[www.pifinancial.com.ar](https://www.pifinancial.com.ar)
No te desanimes si lo visitas, estamos trabajando :)

Puedes consultarnos a colabor@pifinancial.com.ar y claro escribenos si deseas colaborar.

# Licencia

GNU LGPLv3 

[Si lo deseas conoce las bondades de esta licencia.](https://choosealicense.com/licenses/lgpl-3.0/)