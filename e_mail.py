import smtplib
from email.mime.text import MIMEText
from time import localtime
from datetime import datetime, date, timedelta
import calendar
import constantes

def EnviarEmail(asunto, contenido, destinatarios):
    try:
        smtp_ssl_host = 'smtp.gmail.com'  # smtp.mail.yahoo.com
        smtp_ssl_port = 465
        username = constantes.EMAIL_USERNAME
        password = constantes.EMAIL_PASSWORD
        sender = constantes.EMAIL_SENDER
        targets = []

        for d in destinatarios:
            targets.append(d)

        msg = MIMEText(str(contenido))
        msg['Subject'] = asunto
        msg['From'] = sender
        msg['To'] = ', '.join(targets)

        server = smtplib.SMTP_SSL(smtp_ssl_host, smtp_ssl_port)
        server.login(username, password)
        server.sendmail(sender, targets, msg.as_string())
        server.quit()
        #print(msg['To'])
    except:
        print("e_mail.py -- Error al enviar email...")

