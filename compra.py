import funciones
import log
import constantes

class Compra:
    def __init__(self, precio, cantMonedaOperada, tipo_compra, fee):
        self.precio = float(precio)
        self.precio_venta = 0
        self.dineroInvertidoEnCompra = cantMonedaOperada * self.precio
        self.porcentajeDeGananciaDeOperacion = 0
        self.dineroObtenidoPorVenta = 0
        self.fecha_de_compra = funciones.FechaYHora()
        self.fecha_de_venta = ""
        self.tipo_compra = tipo_compra
        self.tipo_venta = ""
        self.fee = fee
        self.cantBTC = (cantMonedaOperada - (cantMonedaOperada * self.fee))
        self.tick_compra = ""
        self.tick_venta = ""
    
    def get_cantBTC(self):
        return self.cantBTC
    
    def get_precio(self):
        return self.precio

    def get_dinero_invertido_en_compra(self):
        return self.dineroInvertidoEnCompra
    
    def get_tipo_compra(self):
        return self.tipo_compra

    def get_tipo_venta(self):
        return self.tipo_venta
    
    def get_precio_venta(self):
        return self.precio_venta

    def get_dinero_obtenido_por_venta(self):
        return self.dineroObtenidoPorVenta
    
    def get_porcentaje_ganancia_operacion(self):
        return self.porcentajeDeGananciaDeOperacion
    
    def get_ganancia_operacion(self):
        return self.dineroObtenidoPorVenta - self.dineroInvertidoEnCompra

    def calcularPorcentajeDeGananciaDeOperacion(self, last_price):
        self.dineroObtenidoPorVenta = (float(last_price) * self.cantBTC) - ((float(last_price) * self.cantBTC) * self.fee)
        return funciones.CalcularPorcentaje(self.dineroInvertidoEnCompra, self.dineroObtenidoPorVenta)

    def CerrarCompra(self, precioVenta, tipo_venta):
        try:
            self.precio_venta = precioVenta
            self.dineroObtenidoPorVenta = (float(precioVenta) * self.cantBTC) - ((float(precioVenta) * self.cantBTC) * self.fee)   # Deberia sumar al capital el precio de venta multiplicado los btc por operacion
            self.porcentajeDeGananciaDeOperacion = funciones.CalcularPorcentaje(self.dineroInvertidoEnCompra, self.dineroObtenidoPorVenta)
            self.fecha_de_venta = funciones.FechaYHora()
            self.tipo_venta = tipo_venta
        except:
            log.registrarEnLog("Error en funcion: (CerrarCompra), del bot:" , "tiker pendiente")