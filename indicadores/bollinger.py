import statistics as stats

def bandasDeBollinger(lista):
    bandas = [0, 0, 0]

    media = stats.mean(lista)
    desviacionEstandar = stats.pstdev(lista)

    bandas = [(media + (2 * desviacionEstandar)), media, (media - (2 * desviacionEstandar))]
    return bandas