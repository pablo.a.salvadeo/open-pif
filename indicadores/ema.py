def compute_ema(smoothing_factor, points):
    """
    Compute exponential moving average of a list of points.
    :param float smoothing_factor: the smoothing factor.
    :param list points: the data points.
    :return list: all ema in a list.
    """
    ema = []
    # The initial point has a ema equal to itself.
    if(len(points) > 0):
        ema.append(points[0])
    for i in range(1, len(points)):
        ema.append(smoothing_factor * points[i] + (1 - smoothing_factor) * ema[i - 1])
    return ema[-1]