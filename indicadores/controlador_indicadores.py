from indicadores import bollinger
from indicadores import ema
from indicadores import media
import math
import random

class Controlador_indicadores:
    def __init__(self):
        self.rsi = 0
        self.media = 0
        self.bandasDeBollinger = 0
        self.ema = 0
        self.rsi = 0
        self.porcentajeDeBolinger = 0

    def calcularIndicadores(self, precios, precios_para_ema, precios_para_bolinger, factorDeSuavisado = 0.2149):
        self.media = media.Media(precios)
        self.bandasDeBollinger = bollinger.bandasDeBollinger(precios_para_bolinger)
        self.ema = ema.compute_ema(factorDeSuavisado, precios_para_ema)
        self.porcentajeDeBolinger = ((self.bandasDeBollinger[0] - precios[-1]) * 100) / (self.bandasDeBollinger[0] + 0.000001 - self.bandasDeBollinger[2])

    def getRsi(self):
        return self.rsi
    def getMedia(self):
        return self.media
    def getBandasBollinger(self):
        return self.bandasDeBollinger
    def getEma(self):
        return self.ema
    def getPorcentageDeBolinger(self):
        return self.porcentajeDeBolinger   
    def mostrarIndicadores(self):
        print("\nIndicadores: \n")
        print("Media: " + str(self.getMedia()))
        print("EMA: " + str(self.getEma()))
        print("Bandas de Bollinger: " + str(self.getBandasBollinger()))
        print("Porcentaje de Bollinger: " + str(self.getPorcentageDeBolinger()))
    def getCadenaInfoIndicadores(self):
        cadena = ""
        cadena += "\n****Indicadores**** \n"
        cadena += "\nEMA: " + str(self.ema) + "\n"
        cadena += "\nMedia: " + str(self.media) + "\n"
        cadena += "\nBandas de Bollinger: " + str(self.bandasDeBollinger) + "\n"
        cadena += "\nPorcentaje de Bollinger: " + str(self.porcentajeDeBolinger) + "\n"
        return cadena
