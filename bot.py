from datetime import datetime, date, timedelta
from registro_operaciones import registro_operaciones
from graficador import Graficador
import constantes
import bd_bots
from bot_telegram import telegram_bot

class bot():   
    def __init__(self, nombre, algoritmo, trader, billetera):     
        self.nombre = nombre
        self.algoritmo = algoritmo
        self.trader = trader
        self.billetera = billetera
        self.algoritmo.añadir_bot(self)
        self.algoritmo.obs_de_mercado.añadir_bot(self)
        self.billetera.añadir_bot(self)
        self.billetera.check_compra_inicial()
        self.registrador = registro_operaciones(self)
        self.graficador = Graficador()
        self.pausa = False
        self.pausa_por_ganancia = False
        self.telegram_bot = telegram_bot(self)
        
    def bot_ejecutar_algoritmo_15_min(self):
        respuesta_algoritmo = self.algoritmo.ejecutar_algoritmo_15_min()
        if (respuesta_algoritmo != None and respuesta_algoritmo[0] == "compra" and not self.pausa):
            self.bot_comprar(respuesta_algoritmo[1])
        if (respuesta_algoritmo != None and respuesta_algoritmo[0] == "venta" and not self.pausa):
            self.bot_vender(respuesta_algoritmo[1])
        
    
    def bot_ejecutar_algoritmo_5_min(self):
        respuesta_algoritmo = self.algoritmo.ejecutar_algoritmo_5_min()
        if (respuesta_algoritmo != None and respuesta_algoritmo[0] == "compra" and not self.pausa):
            self.bot_comprar(respuesta_algoritmo[1])
        if (respuesta_algoritmo != None and respuesta_algoritmo[0] == "venta" and not self.pausa):
            self.bot_vender(respuesta_algoritmo[1])
        self.bot_actualizar_bd()
        self.revisar_inicio_semana()
    
    def bot_actualizar_bd(self):
        capital_entrante = bd_bots.get_capital_entrante(self.nombre)
        if (capital_entrante > 0):
            capital_disponible_actual = self.billetera.get_capital_disponible()
            self.billetera.set_capital_disponible(capital_disponible_actual + capital_entrante)
            bd_bots.update_capital_entrante(self.nombre, 0)
            
        bd_bots.update_capital_disponible(self.nombre, self.billetera.get_capital_disponible())
        bd_bots.update_capital_moneda_secundaria(self.nombre, self.billetera.get_capital_moneda_secundaria())
        bd_bots.update_cant_op_realizadas(self.nombre, self.billetera.get_cant_op_realizadas())
        bd_bots.update_capital_total(self.nombre, self.billetera.get_capital_total(self.algoritmo.obs_de_mercado.get_ultimo_precio()))
        if(self.billetera.compra_abierta()):
            bd_bots.update_compra(self.nombre, self.billetera.get_compra_para_bd())
        else:
            bd_bots.update_compra(self.nombre,[])
    
    def bot_comprar(self, tipo_compra):
        if(self.billetera.compra == []):
            cantidad_a_comprar = self.billetera.get_monto_por_operacion(self.algoritmo.obs_de_mercado.get_ultimo_precio())
            compra = self.trader.comprar(cantidad_a_comprar)
            if(compra != False):
                self.billetera.registrarCompraRealizada(compra, cantidad_a_comprar, tipo_compra)
                self.bot_actualizar_bd()
    
    def bot_vender(self, tipo_venta):
        if(self.billetera.compra != []):
            venta = self.trader.vender(self.billetera.compra[-1].get_cantBTC())
            if (venta != False):
                self.billetera.registrarVentaRealizada(venta, tipo_venta)
                self.bot_actualizar_bd()

    ##################################################################################

    def graficar_resultado(self):
        self.graficador.graficar_resultado("ticks", "USD", self.get_nombre(), self.billetera.get_capital_inicial())

    def graficar_puntos(self):
        self.graficador.graficar_puntos_compra_venta("ticks", "USD")

    def get_nombre(self):
        return self.nombre
    
    def bot_pausar(self):
        self.pausa = True
        self.telegram_bot.enviar_mensaje_suscriptores("Se puso en pausa el bot manualmente por uno de los administradores.")
    
    def bot_pausar_por_ganancia(self):
        self.pausa = True
        self.pausa_por_ganancia = True
        self.telegram_bot.enviar_mensaje_suscriptores("Se alcanzo el nivel de ganancia semanal establecido y el bot se ha pausado.")
    
    def bot_despausar(self):
        self.pausa = False
        self.pausa_por_ganancia = False
        self.telegram_bot.enviar_mensaje_suscriptores("Se despauso el bot manualmente por uno de los administradores.")

    def revisar_inicio_semana(self):
        if(int(datetime.isoweekday(datetime.today())) == constantes.DIA_REPORTE and int(datetime.today().hour) == constantes.HORA_REPORTE and self.billetera.bandera_capital_inicio_semana == False):
            self.billetera.capital_inicio_semana = self.billetera.get_capital_total(self.algoritmo.obs_de_mercado.get_ultimo_precio())
            bd_bots.update_capital_inicio_semana(self.nombre, self.billetera.capital_inicio_semana)
            self.billetera.bandera_capital_inicio_semana == True
            if (self.pausa_por_ganancia):
                self.bot_despausar()

        if(int(datetime.isoweekday(datetime.today())) == 2 and self.billetera.bandera_capital_inicio_semana == True):
            self.billetera.bandera_capital_inicio_semana == False

    def ejecutar_final_backtest(self):
        self.billetera.print_info_final(self.algoritmo.obs_de_mercado.get_ultimo_precio())
        self.graficar_resultado()
        self.graficar_puntos()
        self.registrador.archivo.close()
    
    def inicializar_para_backtest(self):
        bd_bots.update_compra(self.nombre,[])
        self.billetera.compra = []
        bd_bots.update_capital_inicial(self.nombre,100)
        bd_bots.update_capital_disponible(self.nombre, 100)
        bd_bots.update_capital_inicio_semana(self.nombre, 100)
        bd_bots.update_capital_moneda_secundaria(self.nombre, 0)
        bd_bots.update_cant_op_realizadas(self.nombre, 0)



        