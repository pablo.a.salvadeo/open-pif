import urllib.request
import json
from time import localtime
import time
import e_mail
from funciones import FechaYHora
import os
import threading
import constantes
from datetime import datetime
import math

class cargador_historico:
    def __init__(self, exchange, tiker):
        self.url_ohlc = "https://api.cryptowat.ch/markets/" + exchange + "/" + tiker + "/ohlc"
        self.tiker = tiker
        self.periodo = "300"  # 300 = 5min
        self.unix_rango = 2232000
        self.fecha_desde = ''
        self.unix_timestamp_desde = ''
        self.unix_timestamp_final = ''
        self.unix_timestamp_hasta = '' # 1 mes
        self.lista_precios = []
        self.archivo = open('market-price.csv','w')
        self.url_ohlc = "https://poloniex.com/public?command=returnChartData&currencyPair=" + self.tiker + "&start=" + str(self.unix_timestamp_desde) + "&end=" + str(self.unix_timestamp_hasta) + "&period=" + self.periodo

    def pedir_lista_precios(self, desde, periodo, final = ""):
        self.periodo = periodo
        self.fecha_desde = desde
        if (final != ""):
            self.unix_timestamp_final = math.trunc(datetime.timestamp(datetime.strptime(str(final), '%Y-%m-%d %H:%M:%S')))
        self.unix_timestamp_desde = math.trunc(datetime.timestamp(datetime.strptime(self.fecha_desde, '%Y-%m-%d %H:%M:%S')))
        self.unix_timestamp_hasta = self.unix_timestamp_desde + self.unix_rango
        while(True):
            try:
                self.url_ohlc = "https://poloniex.com/public?command=returnChartData&currencyPair=" + self.tiker + "&start=" + str(self.unix_timestamp_desde) + "&end=" + str(self.unix_timestamp_hasta) + "&period=" + self.periodo
                response = urllib.request.urlopen(self.url_ohlc)

                data = json.loads(response.read().decode())
                for d in data:
                    if(d['close'] == 0):
                        break
                    self.archivo.write('{},{}\n'.format(d['date'],d['close']))
                    
                if(self.unix_timestamp_hasta > math.trunc(datetime.timestamp(datetime.now()))):
                    break
                self.unix_timestamp_desde = self.unix_timestamp_desde + self.unix_rango
                self.unix_timestamp_hasta = self.unix_timestamp_hasta + self.unix_rango
            except:
                print("Fin")
                break

        self.archivo.close()
        print("\n***** Lista descargada correctamente y guardada en 'market-price.csv' ******\n")


