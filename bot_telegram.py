import bd_bots
import telebot
from telebot import types
import threading
import time
import funciones


class telegram_bot:
    def __init__(self, bot):
        self.bot = bot
        self.hiloBotTelegram = threading.Thread()
        self.iniciarHiloBotTelegram()
        self.hiloComprobadorErrores = threading.Thread(target= self.comprobarEstadoHiloYReparar)
        self.hiloComprobadorErrores.start()

    def comprobarEstadoHiloYReparar(self):   
        while True:
            if (self.hiloBotTelegram.is_alive() == False):
                self.iniciarHiloBotTelegram()
                print(self.hiloBotTelegram.getName() + " reparado.")
            time.sleep(10)

    def iniciarHiloBotTelegram(self):
        self.hiloBotTelegram = threading.Thread(target= self.bot_telegram)
        self.hiloBotTelegram.setName("HiloBotTelegram")
        self.hiloBotTelegram.start()
        print("Hilo bot telegram activo.")
    
    def enviar_mensaje_suscriptores(self, cadena):
        telegram_bot = telebot.TeleBot(bd_bots.get_telegram_api_key(self.bot.get_nombre()))
        lista_suscriptores = bd_bots.get_suscriptores_telegram(self.bot.nombre)
        for s in lista_suscriptores:
            telegram_bot.send_message(s, cadena)

    def bot_telegram(self):
        telegram_bot = telebot.TeleBot(bd_bots.get_telegram_api_key(self.bot.get_nombre())) 

        @telegram_bot.message_handler(commands=["suscribirse"])
        def suscribir_a_telegram(message):
            cadena = ""
            chatid = message.chat.id
            lista_suscriptores = bd_bots.get_suscriptores_telegram(self.bot.nombre)
            if(lista_suscriptores.count(chatid) == 0):
                lista_suscriptores.append(chatid)
                bd_bots.update_suscriptores_telegram(self.bot.nombre, lista_suscriptores)
                cadena = "Se ha suscrito correctamente al reporte de telegram..."
            else:
                print("Ya existe en la base de datos de suscriptores de telegram..")
                cadena = "Ya existe en la base de datos de suscriptores de telegram.."
            telegram_bot.send_message(chatid, cadena)

        @telegram_bot.message_handler(commands=["info"])
        def info(message):
            chatid = message.chat.id
            nombre_bot = self.bot.get_nombre()
            cadena = nombre_bot + "\n"
            cadena += "Pausa: " + str(self.bot.pausa) + "\n"
            cadena += "Ticker usado: " + bd_bots.get_ticker(nombre_bot) + "\n"
            cadena += "Algoritmo usado: " + bd_bots.get_nombre_algoritmo(nombre_bot) + "\n"
            cadena += self.bot.billetera.get_cadena_info()
            cadena += "Compra abierta: " + str(self.bot.billetera.compra_abierta())

            telegram_bot.send_message(chatid, cadena)

        @telegram_bot.message_handler(commands=["info_compra"])
        def info_compra(message):
            chatid = message.chat.id
            nombre_bot = self.bot.get_nombre()
            cadena = nombre_bot + "\n"
            cadena += self.bot.billetera.get_info_compra_para_telegram()

            telegram_bot.send_message(chatid, cadena)
        
        @telegram_bot.message_handler(commands=["comprar"])
        def comprar(message):
            chatid = message.chat.id
            nombre_bot = self.bot.get_nombre()
            cadena = ""
            if(self.bot.billetera.compra_abierta()):
                cadena = "Ya existe una compra abierta..."
            else:
                self.bot.bot_comprar("Compra desde Telegram")
                cadena = nombre_bot + "Compra realizada correctamente\n"
            telegram_bot.send_message(chatid, cadena)
        
        @telegram_bot.message_handler(commands=["vender"])
        def vender(message):
            chatid = message.chat.id
            nombre_bot = self.bot.get_nombre()
            cadena = ""
            if(not self.bot.billetera.compra_abierta()):
                cadena = "No existe compra abierta..."
            else:
                self.bot.bot_vender("Venta desde Telegram")
                cadena = nombre_bot + "Venta realizada correctamente\n"
            telegram_bot.send_message(chatid, cadena)
        
        @telegram_bot.message_handler(commands=["activar_buscapiso"])
        def activar_buscapiso(message):
            chatid = message.chat.id
            nombre_bot = self.bot.get_nombre()
            cadena = ""
            if(self.bot.billetera.compra_abierta()):
                self.bot.bot_vender("Venta desde Telegram para iniciar busca piso")
                self.bot.algoritmo.mecanismo_compra_virtual.mecanismo_encendido = True
                cadena += nombre_bot + "\nVenta realizada correctamente \n"
                cadena += "Busca piso activado correctamente \n"
            else:
                self.bot.algoritmo.mecanismo_compra_virtual.mecanismo_encendido = True
                cadena = nombre_bot + "\nBusca piso activado correctamente \n"
            telegram_bot.send_message(chatid, cadena)

        @telegram_bot.message_handler(commands=["desactivar_buscapiso"])
        def desactivar_buscapiso(message):
            chatid = message.chat.id
            nombre_bot = self.bot.get_nombre()
            cadena = ""
            self.bot.algoritmo.mecanismo_compra_virtual.salir_buscapiso()
            cadena = nombre_bot + "\nBusca piso desactivado correctamente \n"
            telegram_bot.send_message(chatid, cadena)

        @telegram_bot.message_handler(commands=["pausar_bot"])
        def pausar_bot(message):
            chatid = message.chat.id
            nombre_bot = self.bot.get_nombre()
            cadena = ""
            self.bot.bot_pausar()
            cadena = nombre_bot + " pausado correctamente.\n"
            telegram_bot.send_message(chatid, cadena)
        
        @telegram_bot.message_handler(commands=["despausar_bot"])
        def despausar_bot(message):
            chatid = message.chat.id
            nombre_bot = self.bot.get_nombre()
            cadena = ""
            self.bot.bot_despausar()
            cadena = nombre_bot + " despausado correctamente.\n"
            telegram_bot.send_message(chatid, cadena)

        @telegram_bot.message_handler(commands=["info_mecanismos"])
        def info_mecanismos(message):
            chatid = message.chat.id
            cadena = ""
            cadena = self.bot.algoritmo.get_cadena_info_mecanismos()
            telegram_bot.send_message(chatid, cadena)

        @telegram_bot.message_handler(func=lambda message: True)
        def echo_all(message):
            chatid = message.chat.id
            telegram_bot.send_message(chatid, "Este bot no acepta dicho comando. Acepta /info, /info_compra, /comprar, /vender, /activar_buscapiso, /desactivar_buscapiso, /pausar_bot, /despausar_bot, /info_mecanismos, /suscribirse ")
            print("Respuesta predefinida")
        
        telegram_bot.polling()