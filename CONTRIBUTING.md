# Bienvenido a Open PIF

Gracias por interesarte en colaborar. Este documento, como los demás, esta en construcción. :)

Existen innumerables formas en las que puedes colaborar, pero enumeremos algunas:
 
 - Puedes probar el sistema y sugerir mejoras.
 - Si encuentras algún bug avísanos.
 - Colocaremos algunas tareas en una lista por si quieres elegir desde ahí, lo llamamos "delibery de tareas", pronto estará disponible.
 - Claro puedes crear comportamiento para el bot, ve la sección [Mecanismos del comportamiento](CONTRIBUTING.md#mecanismos-del-comportamiento) ;)
 - Traducir la documentación a tu idioma preferido.
 
 Si deseas colaborar escríbirnos a colabor@pifinancial.com.ar
 
 # Mecanismos del comportamiento
 
 La idea es que cuando agregues una funcionalidad que defina el comportamiento del bot lo hagas de manera modular, creando un mecanismo. El mismo se debe documentar e implementar de manera que pueda convivir con otros mecanismos. Se debe incluir una opción de activar o desactivar el mecanismo. Por ejemplo, puedes crear un mecanismo para realizar compras o ventas en ciertas circunstancias llamado Mi_Mecanismo, ese debe ir acompañado de la documentación y opciones para habilitarlo y deshabilitarlo desde el bot. Vamos a agregar ejemplos de esto en el futuro.
 
