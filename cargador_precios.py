import urllib.request
import json
from time import localtime
import time
import e_mail
from funciones import FechaYHora
import os
import threading
import constantes
from datetime import datetime
import math

class cryptowatch:
    def __init__(self, exchange, tiker):
        self.url_ohlc = "https://api.cryptowat.ch/markets/" + exchange + "/" + tiker + "/ohlc"
        self.periodo_5 = "300"  # 300 = 5min
        self.periodo_15 = "900"  # 900 = 15min
        self.unix_timestamp = ""
        self.lista_precios_5 = []
        self.lista_precios_15 = []
    

    def pedir_lista_precios_5_min(self):
        self.unix_timestamp = math.trunc(datetime.timestamp(datetime.now())) - 21000
        response = urllib.request.urlopen(self.url_ohlc + "?after=" + str(self.unix_timestamp) + "&periods=" + str(self.periodo_5))
        data = json.loads(response.read().decode())
        for d in data['result'][self.periodo_5]:
            self.lista_precios_5.append(d[4])
        return self.lista_precios_5[:-1]

    def pedir_lista_precios_15_min(self):
        self.unix_timestamp = math.trunc(datetime.timestamp(datetime.now())) - 63000
        response = urllib.request.urlopen(self.url_ohlc + "?after=" + str(self.unix_timestamp) + "&periods=" + str(self.periodo_15))
        data = json.loads(response.read().decode())
        for d in data['result'][self.periodo_15]:
            self.lista_precios_15.append(d[4])
        return self.lista_precios_15[:-1]
