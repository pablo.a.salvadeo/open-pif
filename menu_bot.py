from python_console_menu import AbstractMenu, MenuItem
import os
from funciones import CalcularPorcentaje
import e_mail


class MenuBot(AbstractMenu):
    def __init__(self, bot):
        self.bot = bot
        super().__init__("**** Menu Principal ****")
        

    def initialise(self):
        self.add_menu_item(MenuItem(0, "Menu Principal", lambda: self.menuPrincipal()))
        self.add_menu_item(MenuItem(1, "Comprar", lambda: self.comprar()))
        self.add_menu_item(MenuItem(2, "Vender", lambda: self.vender()))
        self.add_menu_item(MenuItem(3, "Pausar Bot", lambda: self.pausar_bot()))
        self.add_menu_item(MenuItem(4, "Despausar Bot", lambda: self.despausar_bot()))

        #self.add_menu_item(MenuItem(12, "Graficar puntos", lambda: self.ImprimirDatos()))

    def menuPrincipal(self):
        os.system ("cls")

    def comprar(self):
        if(self.bot.billetera.compra_abierta()):
            print("Ya existe una compra abierta...")
        else:
            try:
                os.system ("cls")
                entrada = input("Seguro desea realizar una compra en este momento? (y/n)")
                if(entrada == 'y'):
                    self.bot.bot_comprar("Compra desde Consola")
                    print("Compra realizada correctamente\n")
                    input("Presione cualquier tecla para volver al menu principal...")
                    os.system ("cls")
                else:
                    os.system ("cls")
                    input("Compra no realizada, presione cualquier tecla para volver al menu principal...")
            except:
                print("Error al intentar comprar manualmente desde consola.")
    
    def vender(self):
        if(not self.bot.billetera.compra_abierta()):
            print("No hay compra abierta...")
        else:
            try:
                os.system ("cls")
                entrada = input("Seguro desea realizar una venta en este momento? (y/n)")
                if(entrada == 'y'):
                    self.bot.bot_vender("Venta desde Consola")
                    print("Venta realizada correctamente\n")
                    input("Presione cualquier tecla para volver al menu principal...")
                    os.system ("cls")
                else:
                    os.system ("cls")
                    input("Venta no realizada, presione cualquier tecla para volver al menu principal...")
            except:
                print("Error al intentar comprar manualmente desde consola.")
    
    def pausar_bot(self):
        self.bot.pausa = True
        print("\nBot pausado correctamente \n")
        input("Presione cualquier tecla para volver al menu principal...")
        os.system ("cls")

    def despausar_bot(self):
        self.bot.pausa = False
        print("\nBot despausado correctamente \n")
        input("Presione cualquier tecla para volver al menu principal...")
        os.system ("cls")