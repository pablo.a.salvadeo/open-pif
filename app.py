from bot import bot
from algoritmo import algoritmo
from observador_de_mercado import observador_de_mercado_backtest, observador_de_mercado_bitfinex
from traders_y_apis.traders import trader_backtest, trader_bitfinex_btc_usd
from EstadoDeCuenta import EstadoDeCuenta
from menu_bot import MenuBot
import bd_bots
from compra import Compra
import constantes
from cargador_historico import cargador_historico

#cargador = cargador_historico('bitfinex', 'USDT_BTC')
#cargador.pedir_lista_precios('2020-01-01 00:00:00', '300')

nombre_bot = 'bot1'
ticker = bd_bots.get_ticker(nombre_bot)
nombre_algoritmo = bd_bots.get_nombre_algoritmo(nombre_bot)


observador_de_mercado = observador_de_mercado_backtest()
algoritmo1 = algoritmo(nombre_algoritmo, observador_de_mercado)
trader = trader_backtest(observador_de_mercado)
billetera1 = EstadoDeCuenta()
bot1 = bot(nombre_bot, algoritmo1, trader, billetera1)

if(constantes.DEBUG):
    bot1.inicializar_para_backtest()

observador_de_mercado.comenzar_funcionamiento()
menu1 = MenuBot(bot1)
menu1.display()

