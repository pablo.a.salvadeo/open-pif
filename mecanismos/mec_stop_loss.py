
class mec_stop_loss:   
    def __init__(self, bot):
        self.bot = bot
        self.inhibido = False
        self.stopLose = 2.5

    def ejecutar_mecanismo(self, ganancia_operacion):
        if (ganancia_operacion < -self.stopLose and not self.inhibido):                      
            return ['venta', 'Stop_loss']
