from time import localtime
import json
import urllib.request
import os

def FechaYHora():   
    D = str(localtime()[0]) + "/" + str(localtime()[1]) + "/" + str(localtime()[2]) + "  " + str(localtime()[3]) + ":" + str(localtime()[4]) + ":" + str(localtime()[5]) + " "
    return D

def CalcularPorcentaje(montoInicial, montoFinal):
    return (((montoFinal * 100) / montoInicial) - 100)


def CargarPrimerosDatos():
    # actualizar cada tanto el tiempo inicial para cargar mas rapido
    url = "https://poloniex.com/public?command=returnChartData&currencyPair=" + "USDT_BTC" + "&start=1541980800&end=9999999999&period=900"
    response = urllib.request.urlopen(url)
    data = json.loads(response.read().decode())
    precios = []
    p = []
    i = 0
    for d in data:
        p.append(float(d['close']))   
    for d in range(16):
        i += 1
        precios.append(p[-i])
    precios = list(reversed(precios))

def CargarPrimerosDatosBollinger():
    # actualizar cada tanto el tiempo inicial para cargar mas rapido
    url = "https://poloniex.com/public?command=returnChartData&currencyPair=" + "USDT_BTC" + "&start=1541980800&end=9999999999&period=900"
    response = urllib.request.urlopen(url)
    data = json.loads(response.read().decode())
    preciosBollinger = []
    p = []
    i = 0
    for d in data:
        p.append(float(d['close']))   
    for d in range(20):
        i += 1
        preciosBollinger.append(p[-i])
    preciosBollinger = list(reversed(preciosBollinger))

