import threading
import urllib.request
import json
from time import localtime
import time
import e_mail
from funciones import FechaYHora
import os
import cargador_precios
import bd_bots
import constantes


class observador_de_mercado_bitfinex:   
    def __init__(self): 
        self.tiker = ""
        self.url = "https://api.bitfinex.com/v1/pubticker/"
        self.nombreExchange = "bitfinex"
        self.precios_5_min = []
        self.precios_15_min = []
        self.frecuencia = 5
        self.contadorTiks = -1
        self.contadorTiks_15_min = -1
        self.max_datos = 70
        self.error = False
        self.bot = ""
        self.lista_completa = False
        self.ultimo_minuto_de_lectura = 0
        self.hilo = threading.Thread(target= self.iniciar_funcionamiento)
        self.cargador_precios_iniciales = cargador_precios.cryptowatch(self.nombreExchange, self.tiker)

    def añadir_bot(self, bot):
        self.bot = bot
        self.tiker = bd_bots.get_ticker(self.bot.get_nombre())
        self.cargador_precios_iniciales = cargador_precios.cryptowatch(self.nombreExchange, self.tiker)

    def comenzar_funcionamiento(self):
            self.precios_5_min = self.cargador_precios_iniciales.pedir_lista_precios_5_min()
            self.precios_15_min = self.cargador_precios_iniciales.pedir_lista_precios_15_min()
            self.hilo.start()
    def get_ultimo_precio(self):
        return self.precios_5_min[-1]
    def get_ultimo_precio_15_min(self):
        return self.precios_15_min[-1]
    def get_lista_precios_15_min(self):
        return self.precios_15_min[-16:]
    def get_lista_precios_5_min(self):
        return self.precios_5_min[-16:]
    def get_datos_para_bd(self):
        lista = {
        "ultimo_precio":self.precios_5_min[-1]   
        }
        return lista
    
    def leer_mercado(self):
        url = self.url + self.tiker
        repetir_bucle = True
        while (repetir_bucle == True):             
            try:
                response = urllib.request.urlopen(url)
                data = json.loads(response.read().decode())
                siguienteDato = float(data['last_price'])
                self.agregar_dato_a_lista(siguienteDato)
                if (self.error == True):
                    self.error = False
                    e_mail.EnviarEmail("Servicio Activo Bitfinex" ,FechaYHora() + " El sistema ha vuelto a funcionar", constantes.EMAILS_ADMIN)
                repetir_bucle = False
            except:
                print("Intentando adquirir precio nuevamente...")
                #TO DO : si hay algun error al adquirir precio, se deberia reiniciar la carga de precios historicos completa. como si se arrancara de nuevo el sistema.
                if (self.error == False):
                    e_mail.EnviarEmail("Error Bitfinex" ,FechaYHora() + " Ocurrio un error al intentar adquirir un nuevo precio, reintentando", constantes.EMAILS_ADMIN)
                    self.error = True
                time.sleep(60)

    def agregar_dato_a_lista(self, dato):
        self.precios_5_min.append(dato)
        bd_bots.update_obs_de_mercado(self.bot.get_nombre(), self.get_datos_para_bd())
        if (self.ultimo_minuto_de_lectura == 0 or self.ultimo_minuto_de_lectura == 15 or self.ultimo_minuto_de_lectura == 30 or self.ultimo_minuto_de_lectura == 45):
            self.precios_15_min.append(dato)
        if (len(self.precios_5_min) > self.max_datos):
            self.precios_5_min = self.precios_5_min[1:].copy()
            self.lista_completa = True
        else:
            self.lista_completa = False
        if (len(self.precios_15_min) > (self.max_datos)):
            self.precios_15_min = self.precios_15_min[1:].copy()
            
    def iniciar_funcionamiento(self): # Hilo, aqui se lleva el sincronismo.
        bandera = True
        while True:
            minutos = int(time.localtime()[4])           
            if ((minutos == 0 or minutos == 5 or minutos == 10 or minutos == 15 or minutos == 20 or minutos == 25 or minutos == 30 or minutos == 35 or minutos == 40 or minutos == 45 or minutos == 50 or minutos == 55) and bandera):
                self.ultimo_minuto_de_lectura = minutos
                bandera = False
                self.leer_mercado()
                self.contadorTiks += 1
                self.bot.bot_ejecutar_algoritmo_5_min()
                if((minutos == 0 or minutos == 15 or minutos == 30 or minutos == 45) and self.lista_completa):
                    self.contadorTiks_15_min += 1
                    self.bot.bot_ejecutar_algoritmo_15_min()
            if (minutos == 1 or minutos == 6 or minutos == 11 or minutos == 16 or minutos == 21 or minutos == 26 or minutos == 31 or minutos == 36 or minutos == 41 or minutos == 46 or minutos == 51 or minutos == 56):
                bandera = True
            time.sleep(0.3)


class observador_de_mercado_backtest:   
    def __init__(self): 
        self.tiker = ""
        self.archivo = open('market-price.csv','r')
        self.precios_5_min = []
        self.precios_15_min = []
        self.frecuencia = 5
        self.max_datos = 70
        self.error = False
        self.bot = ""
        self.lista_completa = False
        self.ultimo_minuto_de_lectura = 0
        self.contadorTiks = -1
        self.contadorTiks_15_min = -1
        self.hilo = threading.Thread(target= self.iniciar_funcionamiento)

    def añadir_bot(self, bot):
        self.bot = bot
        self.tiker = bd_bots.get_ticker(self.bot.get_nombre())

    def comenzar_funcionamiento(self):
        self.hilo.start()
    
    def leer_mercado(self):            
        try:
            siguienteDato = float(self.archivo.readline()[11:])
            self.agregar_dato_a_lista(siguienteDato)
            if (self.error == True):
                self.error = False
                e_mail.EnviarEmail("Servicio Activo Bitfinex" ,FechaYHora() + " El sistema ha vuelto a funcionar", constantes.EMAILS_ADMIN)
        except ValueError:
            print("\nFIN")
            self.bot.ejecutar_final_backtest()
            os.system("pause")
    
    def agregar_dato_a_lista(self, dato):
        self.precios_5_min.append(dato)
        if (self.ultimo_minuto_de_lectura == 0 or self.ultimo_minuto_de_lectura == 15 or self.ultimo_minuto_de_lectura == 30 or self.ultimo_minuto_de_lectura == 45):
            self.precios_15_min.append(dato)
        if (len(self.precios_5_min) > self.max_datos):
            self.precios_5_min = self.precios_5_min[1:].copy()
            self.lista_completa = True
        else:
            self.lista_completa = False
        if (len(self.precios_15_min) > (self.max_datos)):
            self.precios_15_min = self.precios_15_min[1:].copy()
    
    def get_ultimo_precio(self):
        return self.precios_5_min[-1]
    def get_ultimo_precio_15_min(self):
        return self.precios_15_min[-1]
    def get_lista_precios_15_min(self):
        return self.precios_15_min[-16:]
    def get_lista_precios_5_min(self):
        return self.precios_5_min[-16:]
    def get_datos_para_bd(self):
        lista = {
        "ultimo_precio":self.precios_5_min[-1]   
        }
        return lista


    def iniciar_funcionamiento(self):
        minutos = -5
        while True:
            minutos += 5
            if (minutos == 60):
                minutos = 0
            self.ultimo_minuto_de_lectura = minutos
            self.leer_mercado()
            bd_bots.update_obs_de_mercado(self.bot.get_nombre(), self.get_datos_para_bd())
            if (self.lista_completa):
                self.contadorTiks += 1
            self.bot.bot_ejecutar_algoritmo_5_min()
            #time.sleep(0.1)

            if((minutos == 0 or minutos == 15 or minutos == 30 or minutos == 45) and self.lista_completa):
                self.contadorTiks_15_min += 1
                self.bot.bot_ejecutar_algoritmo_15_min()
                self.bot.graficador.lista_precios_15_min.append(self.precios_15_min[-1])
                self.bot.graficador.lista_bot_cap_total_por_tick.append(self.bot.billetera.get_capital_total(self.precios_15_min[-1]))
                self.bot.graficador.bandasB_sup.append(self.bot.algoritmo.controlador_indicadores_15_min.getBandasBollinger()[0])
                self.bot.graficador.bandasB_inf.append(self.bot.algoritmo.controlador_indicadores_15_min.getBandasBollinger()[2])
                self.bot.graficador.ema_5.append(self.bot.algoritmo.controlador_indicadores_5_min.ema)
                self.bot.graficador.media_5.append(self.bot.algoritmo.controlador_indicadores_5_min.media)
                