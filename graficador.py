import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import constantes
import threading
## https://xkcd.com/color/rgb/  (colores)

class Graficador:
    def __init__(self):
        self.lista_precios_15_min = [] 
        self.lista_bot_cap_total_por_tick = []
        self.punto_compra_x = []
        self.punto_compra_y = []
        self.punto_venta_x = []
        self.punto_venta_y = []
        self.bandasB_sup = []
        self.bandasB_inf = []
        self.ema_5 = []
        self.media_5 = []
        plt.style.use('dark_background')
        self.hilo = threading.Thread(target= self.graficar_puntos_compra_venta)
        self.iniciar_hilo_graf_interactivo()
        

    def iniciar_hilo_graf_interactivo(self):
        if(constantes.GRAFICADOR_INTERACTIVO):
            plt.ion()
            self.hilo.start()
        else:
            plt.ioff()

    def graficar_lista(self, lista, xlabel, ylabel, nombre_lista):
        plt.ioff()
        plt.title("PI FINANCIAL")
        plt.ylabel(ylabel)
        plt.xlabel(xlabel)
        plt.plot(lista, label = nombre_lista, color = "g" , linewidth=1)
        #ax = plt.gca()
        #ax.set_facecolor('xkcd:black')
        plt.show()

    def graficar_resultado(self, xlabel, ylabel, nombre_lista, cap_inicial):
        plt.ioff()
        plt.title("PI FINANCIAL")
        plt.ylabel(ylabel)
        plt.xlabel(xlabel)
        btc_iniciales_hold = cap_inicial / self.lista_precios_15_min[0]
        hold = [i * btc_iniciales_hold for i in self.lista_precios_15_min]
        plt.plot(hold, label = "hold", color = "g" , linewidth=1)
        plt.plot(self.lista_bot_cap_total_por_tick, label = nombre_lista, color = "y" , linewidth=1)
        plt.legend()
        plt.show()

    def graficar_puntos_compra_venta(self, xlabel, ylabel):
        plt.show()
        if(constantes.GRAFICADOR_INTERACTIVO):
            while(True):
                plt.title("PI FINANCIAL")
                plt.ylabel(ylabel)
                plt.xlabel(xlabel)
                plt.plot(list(range(len(self.lista_precios_15_min))) ,self.lista_precios_15_min , label = "precios_15_min", color = 'xkcd:aqua' , linewidth=1)
                #plt.plot(self.cont_ticks ,self.ema_5, label = " ema_5", color = 'xkcd:orange' , linewidth=0.5)
                #plt.plot(self.precios_15, label = " precios_15", color = 'xkcd:aqua' , linewidth=1)
                #plt.plot(self.bandasB_sup, color = 'xkcd:royal blue', linewidth=0.6)
                #plt.plot(self.bandasB_inf, color = 'xkcd:royal blue', linewidth=0.6)
                #plt.plot(self.media, color = 'xkcd:light lime', linewidth=0.6)
                #plt.plot(self.tick_compra_x, self.tick_compra_y, 'w.', markersize=10, linewidth=2)
                #plt.plot(self.tick_venta_x, self.tick_venta_y, 'r.', linewidth=2)
                #plt.plot(self.punto_venta_forzada_x, self.punto_venta_forzada_y, 'm*', markersize=10, linewidth=2)
                #plt.plot(self.punto_venta_puerta_x, self.punto_venta_puerta_y, 'coral*', markersize=10, linewidth=2)
                #plt.plot(self.punto_venta_extrangulamiento_x, self.punto_venta_extrangulamiento_y, 'coral*', markersize=10, linewidth=2)
                #plt.plot(self.colores_x_juntas, self.colores_y_juntas, 'w.', linewidth=2)
                #plt.plot(self.colores_x_bajista, self.colores_y_bajista, 'w.', linewidth=2)
                plt.legend()
                if(constantes.GRAFICADOR_INTERACTIVO):
                    plt.pause(0.1)
                    plt.cla()
        else:
            plt.title("PI FINANCIAL")
            plt.ylabel(ylabel)
            plt.xlabel(xlabel)
            plt.plot(self.lista_precios_15_min , label = "precios_15_min", color = 'xkcd:aqua' , linewidth=0.5)
            plt.plot(self.punto_compra_x, self.punto_compra_y, 'w.', markersize=10)
            plt.plot(self.punto_venta_x, self.punto_venta_y, 'r.', markersize=8)
            plt.plot(self.bandasB_sup, color = 'xkcd:orange', linewidth=0.3)
            plt.plot(self.bandasB_inf, color = 'xkcd:orange', linewidth=0.3)
            plt.plot(self.ema_5, label = " EMA_5_min", color = 'xkcd:fluorescent green' , linewidth=0.3)
            plt.plot(self.media_5, label = " MEDIA_5_min", color = 'xkcd:medium purple' , linewidth=0.3)
            #plt.plot(self.cont_ticks ,self.precios, label = " media_5", color = 'xkcd:bluey purple' , linewidth=0.5)
            #plt.plot(self.cont_ticks ,self.ema_5, label = " ema_5", color = 'xkcd:orange' , linewidth=0.5)
            #plt.plot(self.precios_15, label = " precios_15", color = 'xkcd:aqua' , linewidth=1)
            #plt.plot(self.bandasB_sup, color = 'xkcd:royal blue', linewidth=0.6)
            #plt.plot(self.bandasB_inf, color = 'xkcd:royal blue', linewidth=0.6)
            #plt.plot(self.media, color = 'xkcd:light lime', linewidth=0.6)
            #plt.plot(self.tick_compra_x, self.tick_compra_y, 'w.', markersize=10, linewidth=2)
            #plt.plot(self.tick_venta_x, self.tick_venta_y, 'r.', linewidth=2)
            #plt.plot(self.punto_venta_forzada_x, self.punto_venta_forzada_y, 'm*', markersize=10, linewidth=2)
            #plt.plot(self.punto_venta_puerta_x, self.punto_venta_puerta_y, color = 'orange', marker = 'D', markersize=5, linewidth=0)
            #plt.plot(self.punto_venta_extrangulamiento_x, self.punto_venta_extrangulamiento_y, color = 'orange', marker = '*', markersize=10, linewidth=0)
            #plt.plot(self.colores_x_juntas, self.colores_y_juntas, 'w.', linewidth=2)
            #plt.plot(self.colores_x_bajista, self.colores_y_bajista, 'w.', linewidth=2)
            plt.legend()
            plt.show()
