import funciones
import log
import constantes
import os.path


class registro_operaciones:
    def __init__(self, bot):
        self.bot = bot
        self.nombre_bot = bot.get_nombre()
        self.file_name = "("+ self.nombre_bot + ") " + 'registro_operaciones.txt'
        if (os.path.isfile(self.file_name)):
            self.archivo = open("("+ self.nombre_bot + ") " + 'registro_operaciones.txt','a')
        else:
            self.archivo = open("("+ self.nombre_bot + ") " + 'registro_operaciones.txt','w')

    def registrar_operacion(self, cadena):
        self.archivo = open("("+ self.nombre_bot + ") " + 'registro_operaciones.txt','a')
        self.archivo.write("**************************************************************************\n")
        self.archivo.write(funciones.FechaYHora() + str("\n"))
        self.archivo.write(cadena + "\n")
        self.archivo.write("**************************************************************************\n")
        self.archivo.close()