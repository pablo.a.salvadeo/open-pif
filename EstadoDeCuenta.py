from funciones import CalcularPorcentaje
import log
from compra import Compra
import constantes
import bd_bots
import funciones

class EstadoDeCuenta:
    def __init__(self):
        self.nombreMonedaSecundaria = ""
        self.capital_inicial = 0
        self.capitalDisponible = 0
        self.capitalMonSecundaria = 0
        self.cant_op_realizadas = 0
        self.porcentajeRecaudacion = 3
        self.capitalRecaudado = 0
        self.capital_inicio_semana = 0
        self.bandera_capital_inicio_semana = False
        self.compra = []
        self.historial_compras_cerradas = []
        self.buffer_historial_compras = 10
        self.bot = ""

    def print_balance(self):
        print("Capital moneda secundaria: " + str('%.4f'%self.capitalMonSecundaria) + " " + self.nombreMonedaSecundaria + " ||" + " Capital disponible: " + str('%.2f'%self.capitalDisponible) + " USD")
    
    def get_capital_total(self, precioActual):
        return (self.capitalDisponible + (self.capitalMonSecundaria * precioActual))
    
    def get_capital_disponible(self):
        return self.capitalDisponible
    
    def set_capital_disponible(self, capital):
        self.capitalDisponible = capital

    def get_capital_moneda_secundaria(self):
        return self.capitalMonSecundaria
    
    def get_cant_op_realizadas(self):
        return self.cant_op_realizadas

    def print_info_final(self, precioActual):
        print("Capital moneda secundaria: " + str('%.4f'%self.capitalMonSecundaria) + " " + self.nombreMonedaSecundaria + " ||" + " Capital disponible: " + str('%.2f'%self.capitalDisponible) + " USD")
        print("Ganancia total: " + str(round(self.getPorcentajeGananciaTotal(precioActual), 2)) + " %")

    def get_cadena_info(self):
        cadena = ''
        cadena += "Capital inicial: " + str(self.capital_inicial) + " $\n"
        cadena += "Capital total: " + str(round(self.get_capital_total(self.bot.algoritmo.obs_de_mercado.get_ultimo_precio()), 2)) + " $\n"
        cadena += "Ganancia semanal: " + str(round(self.get_porcentaje_ganancia_semanal(self.bot.algoritmo.obs_de_mercado.get_ultimo_precio()), 2)) + " %\n"
        cadena += "Ganancia total: " + str(round(self.getPorcentajeGananciaTotal(self.bot.algoritmo.obs_de_mercado.get_ultimo_precio()), 2)) + " %\n"
        return cadena

    def getPorcentajeGananciaTotal(self, precioActual):
        return CalcularPorcentaje(self.capital_inicial, (self.capitalDisponible + (self.capitalMonSecundaria * float(precioActual))))
    
    def get_porcentaje_ganancia_semanal(self, precioActual):
        return CalcularPorcentaje(self.capital_inicio_semana, (self.capitalDisponible + (self.capitalMonSecundaria * float(precioActual))))
    
    def get_ultimo_precio_compra(self):
        if(self.compra != []):
            return self.compra[-1].precio
    
    def get_capital_inicial(self):
        return self.capital_inicial
    
    def compra_abierta(self):
        if (len(self.compra) > 0):
            return True
        else: return False
    
    def get_compra_para_bd(self):
        lista = {
        "precio":self.compra[-1].precio,
        "cantBTC":self.compra[-1].cantBTC,
        "tipo":self.compra[-1].tipo_compra,   
        "fee":self.compra[-1].fee    
        }
        return lista

    def recaudar(self):
        if (CalcularPorcentaje(self.capital_inicial, self.capitalDisponible) > self.porcentajeRecaudacion):
            self.capitalRecaudado += self.capitalDisponible - self.capital_inicial
            self.capitalDisponible = self.capital_inicial
            print("Recaudado")

    def get_monto_por_operacion(self,precioActual):
        monto = (self.capitalDisponible / precioActual)
        return (monto - (monto * 0.01))

    def get_info_compra_para_telegram(self):
        if(self.compra_abierta()):
            cadena = "Compra abierta\n"
            cadena += self.compra[-1].fecha_de_compra + "\n"
            cadena += "Cantidad: " + str(round(self.compra[-1].get_cantBTC(),5)) + " " + self.nombreMonedaSecundaria + "\n"
            cadena += "Precio: " + str(round(self.compra[-1].get_precio(),2)) + " USD\n"
            cadena += "Total invertido: " + str(round(self.compra[-1].get_dinero_invertido_en_compra(),2)) + " USD\n"
            cadena += "Ganancia de operacion: " + str(round(self.compra[-1].calcularPorcentajeDeGananciaDeOperacion(self.bot.algoritmo.obs_de_mercado.get_ultimo_precio()), 2)) + " %\n"
            cadena += "Tipo: " + str(self.compra[-1].get_tipo_compra()) + "\n"
            cadena += "Nº tick compra: " + str(self.compra[-1].tick_compra)
            return cadena
        else:
            try:
                cadena = "Compra cerrada\n"
                cadena += self.historial_compras_cerradas[-1].fecha_de_venta + "\n"
                cadena += "Cantidad: " + str(round(self.historial_compras_cerradas[-1].get_cantBTC(),5)) + " " + self.nombreMonedaSecundaria + "\n"
                cadena += "Precio: " + str(round(self.historial_compras_cerradas[-1].get_precio(),2)) + " USD\n"
                cadena += "Total invertido: " + str(round(self.historial_compras_cerradas[-1].get_dinero_invertido_en_compra(),2)) + " USD\n"
                cadena += "Ganancia de operacion: " + str(round(self.historial_compras_cerradas[-1].get_porcentaje_ganancia_operacion(), 2)) + " %\n"
                cadena += "Tipo compra: " + str(self.historial_compras_cerradas[-1].get_tipo_compra()) + "\n"
                cadena += "Tipo venta: " + str(self.historial_compras_cerradas[-1].get_tipo_venta()) + "\n"
                cadena += "Nº tick venta: " + str(self.historial_compras_cerradas[-1].tick_venta)
                return cadena
            except:
                print("Error telegram (info compra cerrada)")
                return "Error telegram (info compra cerrada)"
    
    def get_cadena_info_compra(self):
        cadena = "Compra\n"
        cadena += "Cantidad: " + str(round(self.compra[-1].get_cantBTC(),5)) + " " + self.nombreMonedaSecundaria + "\n"
        cadena += "Precio: " + str(round(self.compra[-1].get_precio(),2)) + " USD\n"
        cadena += "Total invertido: " + str(round(self.compra[-1].get_dinero_invertido_en_compra(),2)) + " USD\n"
        cadena += "Tipo: " + str(self.compra[-1].get_tipo_compra()) + "\n"
        cadena += "Nº tick: " + str(self.bot.algoritmo.obs_de_mercado.contadorTiks_15_min)
        return cadena
    
    def get_cadena_info_venta(self):
        cadena = "Venta\n"
        cadena += "Cantidad: " + str(round(self.compra[-1].get_cantBTC(),5)) + " " + self.nombreMonedaSecundaria + "\n"
        cadena += "Precio: " + str(round(self.compra[-1].get_precio_venta(),2)) + " USD\n"
        cadena += "Total obtenido: " + str(round(self.compra[-1].get_dinero_obtenido_por_venta(),2)) + " USD\n"
        cadena += "Ganancia: " + str(round(self.compra[-1].get_ganancia_operacion(),2)) + " USD\n"
        cadena += "Porcentaje de ganancia: " + str(round(self.compra[-1].get_porcentaje_ganancia_operacion(),2)) + " %\n"
        cadena += "Tipo: " + str(self.compra[-1].get_tipo_venta()) + "\n"
        cadena += "Nº tick: " + str(self.bot.algoritmo.obs_de_mercado.contadorTiks_15_min)
        return cadena


    def registrarCompraRealizada(self, precioOperado, cantMonedaOperada, tipo):
        try:
            self.compra.append(Compra(precioOperado, cantMonedaOperada, tipo, self.bot.trader.get_fee()))
            self.capitalDisponible -= (self.compra[-1].cantBTC * self.compra[-1].precio)
            self.capitalMonSecundaria += self.compra[-1].get_cantBTC()
            self.cant_op_realizadas += 1
            self.compra[-1].tick_compra = self.bot.algoritmo.obs_de_mercado.contadorTiks_15_min
            self.bot.registrador.registrar_operacion(self.get_cadena_info_compra())
            print(tipo)
            if(constantes.DEBUG):
                self.bot.graficador.punto_compra_x.append(self.bot.algoritmo.obs_de_mercado.contadorTiks_15_min)
                self.bot.graficador.punto_compra_y.append(self.compra[-1].precio)
        except:
            log.registrarEnLog("Error al registrar compra realizada", "poner tiker aqui")
    
    def registrarVentaRealizada(self, precioVenta, tipo_venta):
        self.compra[-1].CerrarCompra(precioVenta, tipo_venta)
        self.capitalDisponible += self.compra[-1].get_dinero_obtenido_por_venta()
        self.capitalMonSecundaria -= self.compra[-1].get_cantBTC()
        self.cant_op_realizadas += 1
        self.bot.registrador.registrar_operacion(self.get_cadena_info_venta())
        self.compra[-1].tick_venta = self.bot.algoritmo.obs_de_mercado.contadorTiks_15_min
        self.historial_compras_cerradas.append(self.compra[-1])
        if(len(self.historial_compras_cerradas) > 10):
            self.historial_compras_cerradas.pop(0)
        self.compra = []
        print(tipo_venta)
        if(self.get_porcentaje_ganancia_semanal(precioVenta) > constantes.GANANCIA_SEMANAL_MAXIMA):
            self.bot.bot_pausar_por_ganancia()

        if(constantes.DEBUG):
            self.bot.graficador.punto_venta_x.append(self.bot.algoritmo.obs_de_mercado.contadorTiks_15_min)
            self.bot.graficador.punto_venta_y.append(self.historial_compras_cerradas[-1].precio_venta)

    def añadir_bot(self, bot):
        self.bot = bot
        nombre_bot = self.bot.get_nombre()
        self.cant_op_realizadas = bd_bots.get_cant_op_realizadas(nombre_bot)
        self.capital_inicial = bd_bots.get_capital_inicial(nombre_bot)
        self.capitalDisponible = bd_bots.get_capital_disponible(nombre_bot)
        self.capitalMonSecundaria = bd_bots.get_capital_moneda_secundaria(nombre_bot)
        self.capital_inicio_semana = bd_bots.get_capital_inicio_semana(nombre_bot)
    
    def check_compra_inicial(self):
        compra_inicial = bd_bots.get_compra(self.bot.nombre)
        if(compra_inicial != []):
            print("Compra cargada desde bd")
            cantidad_real = float(compra_inicial["cantBTC"]) + (float(compra_inicial["cantBTC"]) * float(compra_inicial["fee"]))
            compra_inicial = Compra(compra_inicial["precio"], cantidad_real, compra_inicial["tipo"], compra_inicial["fee"])
            self.compra.append(compra_inicial)
   