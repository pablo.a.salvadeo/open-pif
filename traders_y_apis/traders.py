from traders_y_apis import bitfinex_btc_usd
import time
import log

class trader_bitfinex_btc_usd:
    def __init__(self, tiker):
        self.tiker = tiker
        self.fee = 0.002

    def comprar(self, cant):
        precio_operado = self.CrearOrden("buy", "exchange market", cant, str(12333))
        return float(precio_operado)
    
    def vender(self, cant):
        precio_operado = self.CrearOrden("sell", "exchange market", cant, str(12333))
        return float(precio_operado)

    def get_fee(self):
        return self.fee

    def CrearOrden(self, buyOrSell, tipo, monto, precio):
        self.buyOrSell = buyOrSell
        self.tipo = tipo
        self.monto = monto
        self.precio = precio  
        try:
            self.datosOrdenCreada = bitfinex_btc_usd.place_order(str(monto), str(precio), buyOrSell, tipo, self.tiker)
        except:
            time.sleep(30) # 30 seg
            try:
                self.datosOrdenCreada = bitfinex_btc_usd.place_order(str(monto), str(precio), buyOrSell, tipo, self.tiker)
            except:
                time.sleep(90)  # 1 minuto y medio
                try:
                    self.datosOrdenCreada = bitfinex_btc_usd.place_order(str(monto), str(precio), buyOrSell, tipo, self.tiker)
                except:
                    time.sleep(180) # 3 min
                    try:
                        self.datosOrdenCreada = bitfinex_btc_usd.place_order(str(monto), str(precio), buyOrSell, tipo, self.tiker)
                    except:
                        log.registrarEnLog("Error al crear orden, reintentos agotados. Se cancelo la accion.", self.tiker)
                        return False
        
        if "price" in self.datosOrdenCreada:
            try:
                self.status_order = bitfinex_btc_usd.status_order(int(self.datosOrdenCreada["id"]))
            except:
                time.sleep(60) # 1 min
                try:
                    self.status_order = bitfinex_btc_usd.status_order(int(self.datosOrdenCreada["id"]))
                except:
                    time.sleep(600) # 10 min
                    try:
                        self.status_order = bitfinex_btc_usd.status_order(int(self.datosOrdenCreada["id"]))
                    except:
                        time.sleep(3600) # 1 hora
                        try:
                            self.status_order = bitfinex_btc_usd.status_order(int(self.datosOrdenCreada["id"]))
                        except:
                            time.sleep(10800) # 3 horas
                            try:
                                self.status_order = bitfinex_btc_usd.status_order(int(self.datosOrdenCreada["id"]))
                            except:
                                log.registrarEnLog("Error al leer info de operacion creada correctamente, reintentos agotados. ", self.tiker)
                                return False
        else:
            log.registrarEnLog(str(self.datosOrdenCreada), self.tiker)
            return False

        bandera = False
        while (float(self.status_order["avg_execution_price"]) <= 0):
            try:
                self.status_order = bitfinex_btc_usd.status_order(int(self.datosOrdenCreada["id"]))
                if (bandera == True):
                    log.registrarEnLog("Status order leido correctamente.", self.tiker)
                time.sleep(2)
            except:
                print("Error al leer el status de la orden, reintentando...")
                if (bandera == False):
                    log.registrarEnLog("Error al leer el status de la orden, reintentando...", self.tiker)
                    bandera = True
                self.status_order = {"avg_execution_price" : 0}

        self.precio = float(self.status_order["avg_execution_price"])
        #print("Orden creada con precio : " + str(self.precio))
        return self.precio



class trader_backtest: 
    def __init__(self, obs):
        self.observador_de_mercado = obs
        self.fee = 0.002
    
    def comprar(self, cantMonedaSecundariaAComprar):
        precioOperado = self.observador_de_mercado.get_ultimo_precio()
        return float(precioOperado)
    
    def vender(self, cantMonedaSecundariaAVender):
        precioOperado = self.observador_de_mercado.get_ultimo_precio()
        return float(precioOperado)
    
    def get_fee(self):
        return self.fee