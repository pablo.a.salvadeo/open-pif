from time import localtime
import e_mail
import constantes


def FechaYHora():   
    D = str(localtime()[0]) + "/" + str(localtime()[1]) + "/" + str(localtime()[2]) + "  " + str(localtime()[3]) + ":" + str(localtime()[4]) + ":" + str(localtime()[5]) + " "
    return D

def registrarEnLog(texto, tk = "Tiker no especificado"):
    archivoLog = open('log.txt','a')
    archivoLog.write(FechaYHora() + texto + "\n")
    e_mail.EnviarEmail("Log " + tk, texto, constantes.EMAILS_ADMIN) 